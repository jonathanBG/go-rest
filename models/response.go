package models

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type Response struct {
	Status      int         `json:"status"`
	Data        interface{} `json:"data"`
	Message     string      `json:"message"`
	contentType string
	wtriter     http.ResponseWriter
}

func SendUnprocessableEntity(w http.ResponseWriter) {
	res := CreateDefaultReponse(w)
	res.UnprocessableEntity()
	res.Send()
}

func (this *Response) UnprocessableEntity() {
	this.Status = http.StatusUnprocessableEntity
	this.Message = "Unprocessable Entity"
}

func CreateDefaultReponse(w http.ResponseWriter) Response {
	return Response{Status: http.StatusOK, wtriter: w, contentType: "application/json"}
}

func (this *Response) NotFound() {
	this.Status = http.StatusNotFound
	this.Message = "resource not found"
}

func SendData(w http.ResponseWriter, data interface{}) {
	res := CreateDefaultReponse(w)
	res.Data = data
	res.Send()
}

func SendNotFound(w http.ResponseWriter) {
	res := CreateDefaultReponse(w)
	res.NotFound()
	res.Send()
}

func (this *Response) Send() {
	this.wtriter.Header().Set("Content-Type", this.contentType)
	this.wtriter.WriteHeader(this.Status)
	res, _ := json.Marshal(&this)
	fmt.Fprintf(this.wtriter, string(res))
}
