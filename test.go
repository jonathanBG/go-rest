package main

import (
	"fmt"

	"./models"
)

func main() {
	models.CreateConection()
	models.CreateTables()

	user := models.CreateUser("jonathan", "pass123", "jonatha@email.com")
	fmt.Println(user)

	models.CloseConnection()
}
